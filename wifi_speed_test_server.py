import socket
import sys
from optparse import OptionParser, OptionGroup
import signal
import select
import time
from threading import Thread

def_port = 1212

def is_protocol_msg(msg):
    msg = msg.split(' ')
    if len(msg) != 3:
        return False
    if msg[0]!= 'push':
        return False
    try:
        block = int(msg[1])
        size = int(msg[2])
    except Exception, e:
        return False
    return True
	
def get_block_size(msg):
    return int(msg.split(' ')[1])

def get_full_size(msg):
    return int(msg.split(' ')[2])
    
usage = "usage: %prog [options] port"
version = "1.1"
parser = OptionParser(usage, version=version)
parser.add_option("-o", "--output", dest="filename",
                  help="The name of the output file", default = None)
parser.add_option("-d", "--dont-truncate", help = "Do not truncate the output file if it already exists", action="store_true", dest="dont_truncate", default=False)
parser.add_option("-a", "--auto-restart", help = "Restart automatically when the connection closed", action="store_true", dest="auto_restart", default=True)
parser.add_option("-v", "--verbose", help = "Verbose mode", action="store_true", dest="verbose", default=False)
parser.add_option("-b", "--broadcast", dest="broadcast", help="The broadcast IP address", default = None)


slow_receive_group = OptionGroup(parser, 'Options to simulate slow receiver')
slow_receive_group.add_option("-f", "--read-freq", help = "The waiting time between two reads, default: %default", dest="read_freq", default=0, type="float")
slow_receive_group.add_option("-r", "--receive-buf", help = "The size of the receiver buffer, default: %default", dest="rbuf", default=1024, type="int")
parser.add_option_group(slow_receive_group)

(opts, args) = parser.parse_args()

if len(args) < 1:
    port = def_port

try:
    port = int(args[0])
except:
    port = def_port

filename = opts.filename
if opts.dont_truncate:
    truncate = False
else:
    truncate = True
    
if opts.auto_restart:
    auto_restart = True
else:
    auto_restart = False

def log_msg(msg):
    if opts.verbose:
        print msg

def start():
    log_msg("Waiting for an incoming connection")
    received_data = 0
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('', port))
    server_socket.listen(5)

    if opts.filename:
        if truncate:
            fd = open(filename,"wb")
        else:
            fd = open(filename,"ab")

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind(('', port))
    server_socket.listen(5)

    read_list = [server_socket]
    ready = False

    while not ready:
        try:
            readable, writable, errored = select.select(read_list, [], [],1)
        except KeyboardInterrupt:
            server_socket.close()
            if opts.filename:
                fd.close()
            sys.exit()
        for s in readable:
            if s is server_socket:
                client_socket, address = server_socket.accept()
                log_msg("Incoming connection accepted from: %s" % str(address))
                read_list.append(client_socket)
            else:
                data = s.recv(opts.rbuf)
                if data:
                    if is_protocol_msg(data):
                        msg_block_size = get_block_size(data)
                        full_size = get_full_size(data)
                        log_msg("Push mode, starting sending, total data size to send: %s bytes, block size: %s bytes" % (full_size,msg_block_size))
                        sent_data = 0
                        msg = "\n".zfill(msg_block_size)
                        while (sent_data < full_size):
                            try:
                                s.sendall(msg)
                            except Exception, e:
                                return
                            sent_data += len(msg)
                        log_msg ("Sending finished, sent data: %s bytes" % sent_data)
                    else:
                        received_data += len(data)
                    if opts.filename:
                        fd.write(data)
                        fd.flush()
                    else:
                        sys.stdout.write(data)
                    if opts.read_freq > 0:
                        time.sleep(opts.read_freq)
                else:
                    s.close()
                    read_list.remove(s)
            if len(read_list) == 1:
                ready = True

    server_socket.close()
    if received_data > 0:
        log_msg("Received data: %s bytes" % received_data)
    log_msg("Closing connection\n")
    if opts.filename:
        fd.close()
        
def __get_local_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(("google.com", 80))
        ip = s.getsockname()[0]
        s.close()
    except socket.error:
        ip = None
    log_msg("Local ip: %s" % ip)
    return ip
    
def get_broadcast_ip():
    ''' This is a dirty workaround and works only the netmask is /24 
    but there is no better cross-platform option
    without running external commands like ipconfig, or using special libs '''
    
    if opts.broadcast:
        return opts.broadcast

    local_ip = __get_local_ip()
    broadcast = "255.255.255.255"
    if local_ip == None:
        return broadcast
        
    ip_fields = local_ip.split(".")
    broadcast = "%s.%s.%s.255" %(ip_fields[0], ip_fields[1], ip_fields[2])
    return broadcast
     
def start_broadcast(broadcast_ip, port):
    hostname = socket.gethostname()
    hostname = hostname.replace(' ','_')
    version = "1.0"
    type="C"
    brsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    brsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    brsocket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    while (True):
        log_msg("sending broadcast message to %s:%s" % (broadcast_ip, port))
        brsocket.sendto('%s %s %s' % (version, type, hostname), (broadcast_ip, port))
        time.sleep(1)
    brsocket.close()
    
log_msg("Starting in debug mode")
thread = Thread(target = start_broadcast, args = (get_broadcast_ip(), 1212, ))
thread.daemon = True
thread.start()

if not auto_restart:	
    start()
else:
    while(True):
        start()
